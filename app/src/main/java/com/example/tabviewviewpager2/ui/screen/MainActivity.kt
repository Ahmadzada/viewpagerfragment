package com.example.tabviewviewpager2.ui.screen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.tabviewviewpager2.databinding.ActivityMainBinding
import com.example.tabviewviewpager2.ui.screen.fragments.DemoCollectionFragment

const val SIZE = 10

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val fragment = DemoCollectionFragment()
        supportFragmentManager.beginTransaction().add(binding.container.id, fragment).commit()
    }


}


//
//val adapter = ViewPagerAdapter(MockData.getmockImageList(SIZE))
//binding.pager.adapter = adapter
//
//TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
//    tab.text = "tab ${position +1}"
//
//}.attach()