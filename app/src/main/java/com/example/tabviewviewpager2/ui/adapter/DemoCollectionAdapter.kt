package com.example.tabviewviewpager2.ui.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.tabviewviewpager2.data.enums.TabsType
import com.example.tabviewviewpager2.ui.screen.fragments.DemoObjectFragment

class DemoCollectionAdapter(
    val fragment: Fragment,
) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 7

    override fun createFragment(position: Int): Fragment {
        val fragment = DemoObjectFragment()
        fragment.arguments = Bundle().apply {

            putInt(TabsType.TAB.name, position + 1)
        }
        return fragment
    }


}