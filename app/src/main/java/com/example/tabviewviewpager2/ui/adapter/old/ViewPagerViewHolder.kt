package com.example.tabviewviewpager2.ui.adapter.old

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tabviewviewpager2.databinding.ItemViewPagerBinding

class ViewPagerViewHolder(
    viewGroup: ViewGroup,
    val binding: ItemViewPagerBinding = ItemViewPagerBinding.inflate(
        LayoutInflater.from(viewGroup.context),
        viewGroup,
        false
    )
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(imageId: Int) {
        binding.ivViewPager.setBackgroundResource(imageId)
    }
}