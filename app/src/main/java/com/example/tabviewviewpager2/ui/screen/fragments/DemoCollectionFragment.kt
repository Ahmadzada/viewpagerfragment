package com.example.tabviewviewpager2.ui.screen.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.tabviewviewpager2.data.enums.TabsType
import com.example.tabviewviewpager2.databinding.FragmentDemoCollectionBinding
import com.example.tabviewviewpager2.ui.adapter.DemoCollectionAdapter
import com.google.android.material.tabs.TabLayoutMediator

class DemoCollectionFragment() : Fragment() {
    private lateinit var binding: FragmentDemoCollectionBinding
    private lateinit var demoCollectionAdapter: DemoCollectionAdapter
    private lateinit var viewPager: ViewPager2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDemoCollectionBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        demoCollectionAdapter = DemoCollectionAdapter(this)
        viewPager = binding.pager
        viewPager.adapter = demoCollectionAdapter

        TabLayoutMediator(binding.tabLayout, viewPager) { tab, position ->
            tab.text = "${TabsType.TAB.name} ${(position + 1)}"
        }.attach()
    }
}