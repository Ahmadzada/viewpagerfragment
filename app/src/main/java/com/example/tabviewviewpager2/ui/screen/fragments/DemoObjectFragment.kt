package com.example.tabviewviewpager2.ui.screen.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.tabviewviewpager2.data.enums.TabsType
import com.example.tabviewviewpager2.databinding.FragmentDemoObjectBinding

class DemoObjectFragment : Fragment() {
    private lateinit var binding: FragmentDemoObjectBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDemoObjectBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(TabsType.TAB.name) }?.apply {

            binding.txtV.text = getInt(TabsType.TAB.name).toString()
        }
    }

}