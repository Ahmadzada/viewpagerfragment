package com.example.tabviewviewpager2.ui.adapter.old

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ViewPagerAdapter(val imagesList: List<Int>) :
    RecyclerView.Adapter<ViewPagerViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerViewHolder =
        ViewPagerViewHolder(parent)

    override fun onBindViewHolder(holder: ViewPagerViewHolder, position: Int) {
        holder.bind(imagesList[position])
    }

    override fun getItemCount(): Int = imagesList.size
}

