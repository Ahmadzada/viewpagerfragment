package com.example.tabviewviewpager2.data.enums

enum class TabsType {
    TAB{
        override fun toString(): String {
            return "tab"
        }
    }
}