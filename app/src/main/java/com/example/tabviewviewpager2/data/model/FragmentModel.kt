package com.example.tabviewviewpager2.data.model

import androidx.fragment.app.Fragment

data class FragmentModel(
    val id : Int,
    val fragmentTitle : String,
    val fragment : Fragment
)