package com.example.tabviewviewpager2.data.mocks

import com.example.tabviewviewpager2.R

object MockData {
    fun getmockImageList(size: Int): List<Int> {
        val list = mutableListOf<Int>()
        repeat(size) {
            list.add(R.drawable.icon_android)
        }
        return list
    }
}